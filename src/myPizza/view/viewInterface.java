package myPizza.view;

import myPizza.controller.controllerInterface;

public interface viewInterface {

    void setController(final controllerInterface ctrl);

    void setLabel(String string);

}
