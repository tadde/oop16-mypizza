package myPizza.view;

import myPizza.controller.controllerInterface;
import javax.swing.*;

public class view implements viewInterface {

    private controllerInterface ctrl;
    private final JFrame frame = new JFrame();
    private final JPanel panel = new JPanel();
    private final JButton button = new JButton();
    private final JLabel label = new JLabel();

    public view(){

        frame.setTitle("Titolo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.getContentPane().add(panel);
        button.setText("Saluta Crocco");
        panel.add(button);
        frame.setVisible(true);

        button.addActionListener( e -> {
            this.setLabel(this.ctrl.getLabel());
        });

    }

    public void setLabel(String string){
        JOptionPane.showMessageDialog(null, string);
    }

    public void setController(final controllerInterface controller){
        this.ctrl = controller;
    }

}
