package myPizza.controller;

import myPizza.model.model;
import myPizza.model.modelInterface;
import myPizza.view.view;
import myPizza.view.viewInterface;

public class controller implements controllerInterface {

    private modelInterface model;
    private final viewInterface view;

    public controller (final viewInterface view){
        this.view = view;
        this.startView();
    }

    public String getLabel(){
        return this.model.getLabel();
    }

    private void startView(){
        this.view.setController(this);
        this.model = new model();
    }

    public static void main (final String[] args){
        new controller(new view());
    }
}
